<?php

/**
 *
 */
class Pages
{
	static $db;
	static $table;
	static $rootPages;
	static $allPages;

	private static function SetDb()
	{
		//Makes sure that ParseIni is set
		ParseIni::SetIni("./app/config/config.ini");

		//Loads database and table name
		$dbPath = ParseIni::Get("config", "pageDBPath");
		self::$table = ParseIni::Get("config", "pageDBTableName");

		//Creates DB ORM
		self::$db = new DB\SQL("sqlite:" . $dbPath);
	}

	//Returns an array of headers of the root pages
	public static function GetRootPages()
	{
		// If check has already been made, return result
		// if(self::$rootPages) { return self::$rootPages; }

		$pages = self::LoadAllPages();

		foreach((array) $pages as $page)
		{
			if($page->IsRootPage())
			{
				self::$rootPages[] = $page->header;
			}
		}
		return self::$rootPages;
	}

	//Loads all the pages from the database (unless already loaded) and saves as an array of page-objects
	public static function LoadAllPages()
	{

		//Setts databaseconnection if not existing
		if (!self::$db) {
			self::SetDb();
		}
		//Returns cached allPages if found
		if (!empty(self::$allPages)) {
			return self::$allPages;
		}

		//SQL string
		$sql = 'SELECT * FROM ' . self::$table;

		//Result from db
		$result = self::$db->exec($sql);

		//If no results or table not found
		if (!$result) {
			return false;
		}

		self::$allPages = self::ParseAllPages($result);

		return self::$allPages;
	}

	//Checks the database for header, returns id if found
	public static function CheckDBForID($header)
	{
		$pages = self::LoadAllPages();

		foreach ($pages as $page) 
		{
			if($page->header == $header)
			{
				return (int) $page->id;
			}
		}

		return null;
	}

	//Creates an array of page-objects
	//Takes an array of databasdata
	public static function ParseAllPages(array $pages)
	{
		$parsedPages = array();

		foreach ($pages as $page) {
			$tmpPage = new Page();
			$tmpPage->CreateFromPreload($page);

			$parsedPages[$tmpPage->header] = $tmpPage;
		}

		return $parsedPages;
	}

	public static function IsValidParent(string $parent)
	{
		$pages = self::LoadAllPages();

		//if parent is found as header
		foreach ((array)$pages as $page) {
			if ($parent == $page->header) {
				return true;
			}
		}
	}

	public static function GetSortedPageList()
	{
		$pageArray = self::LoadAllPages();
		//If no results were found
		if (!$pageArray) {
			return false;
		}

		$workArray = $pageArray;
		$sortedArray = array();

		do {
			$runs++;

			foreach ($workArray as $key => $page) {
				if ($page->IsRootPage()) {
					$page->level = 1;
					//Adds page to the end of sorted array
					array_splice($sortedArray, 0, 0, array($page));
					unset($workArray[$key]);
					$check++;
				}
				//Checks for the parentarray in the sorted array
				elseif ($parentPos = self::GetPosOfPage($page->parent, $sortedArray)) {
					
					$parentPage = $sortedArray[$parentPos-1];

					//Sets level as 1 above parent.
					$page->level = $parentPage->level + 1;

					array_splice($sortedArray, $parentPos, 0, array($page));
					unset($workArray[$key]);
					$check++;
				} else {
					continue;
				}
			}

			//Checks to make sure to not get into infinity loop, for example if no parent is found
			if (($runs - $check) > 2) {
				foreach ($workArray as $key => $value) {
					var_dump($value->header);
				}
				throw new Exception("Sorting has stallen, aborting");
			}
		} while (!empty($workArray));

		//Returns the array with reset keys to retain order in the JSON-object
		return array_values($sortedArray);
	}

	//Returns position of page in array, if not found returns 0
	private static function GetPosOfPage(string $pageName, $pageArray) : int
	{
		$i = 1;
		foreach ($pageArray as $key => $page) {
			if ($page->header == $pageName) {
				return $i;
			}
			$i++;
		}

		return 0;
	}
}
