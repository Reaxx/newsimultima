<?php

/**
 *
 */
class Page extends DBitems
{
	public $id;
	public $parent;
	public $header;
	public $content;
	public $level;

	public function SetHeader(string $header)
	{
		if(!$header) { throw new Exception("Header cant be empty"); }

		$this->header = $header;
		return true;
	}
	public function SetParent(string $parent)
	{
		if(!$parent) { throw new Exception("Parent cant be empty"); }

		$this->parent = $parent;
		return true;
	}
	public function SetContent(string $content)
	{
		if(!$content) { $this->content = null; }
		else { $this->content = $content; }

		return true;
	}

	//Sets $this->db
	protected function SetDbPath()
	{
		ParseIni::SetIni("./app/config/config.ini");
		$this->dbPath = ParseIni::Get("config","pageDBPath");
	}
	//Sets $this->mapper
	protected function SetTableName()
	{
		$this->tableName = ParseIni::Get("config","pageDBTableName");
	}
	//Sets $this->startSql
	protected function SetStartSql()
	{
		$this->schemaSql =
			'CREATE TABLE IF NOT EXISTS "' . $this->tableName . '" (
			`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
			`header` TEXT NOT NULL UNIQUE, 
			`content` TEXT NOT NULL,
			`parent` TEXT, FOREIGN KEY(`parent`) REFERENCES `pages`(`header`) ON DELETE RESTRICT
			)';
	}
		//Sets the object
	protected function SetData(...$args)
	{

	}

		//Returns false if no data was found
		//Loads with mapper
	public function Load()
	{
			//First tries to load by id
		if (isset($this->id)) {
			$data = $this->LoadByField("id", $this->id);
		}
			//Then by header
		else if (isset($this->header)) {
			$data = $this->LoadByField("header", $this->header);
		} else {
			return 0;
		}

			//Checks for data
		if (!$data) {
			return 0;
		}

			//Maps the data to the object
		$this->id = $data->id;
		$this->header = $data->header;
		$this->parent = $data->parent;
		$this->content = $data->content;

		return true;
	}

	// 	//Needs rework for deeper layouyts
	public function GetRaltedPages()
	{
		if (!$this->related) {
			if ($this->IsRootPage()) {
				$search = $this->header;
			} else {
				$search = $this->parent;
			}

			$sql = 'SELECT DISTINCT  id, parent, header FROM pages WHERE parent= "' . $search . '"';
			$data = $this->db->exec($sql);

			$this->related = $data;
		}
		return $this->related;
	}
	
	public function IsRootPage()
	{
		if ($this->parent != null) {			
			return false;
		}
		// 	//Setts level
		// $this->level = 1;
		return true;
	}

	//Tries to save object from POST-data, if no ID is given inserts
	public function SaveFromPost()
	{
		$this->mapper->copyFrom('POST');
		if(!$this->HasValidParent($this->mapper->parent)) { throw new Exception("Non-valid parent".$this->mapper->parent); }

		if(!$this->mapper->save())
		{
			throw new Exception();
		}
		return true;
	}

	//Checks the database for 
	private function CheckDBForID()
	{
		$data = $this->LoadByField("header", $this->header);
		if(!$data) { return null; }		

		return $data->id;
	}

	public function Save()
	{
		if(!$this->header) { throw new Exception("No header found"); }
		if(!$this->HasValidParent()) { throw new Exception("Non-valid parent"); }

		//Checks if header aldready is in the database, setts mapper to object
		$this->id = $this->CheckDBForID($this->header) ?? null;

		$this->mapper->copyfrom($this);
		
		//If id was found, updates. Otherwise inserts
			if(!$this->mapper->save())
			{
				throw new Exception();
			}
			return true;		
	}

	public function CreateFromPreload($data)
	{
		$this->id = $data["id"];
		$this->header = $data["header"];
		$this->parent = $data["parent"];
		$this->content = $data["content"];
	}

		//For some reason fatfree allows me to add parents that the sql-sceme shouldnt allow, so added a check.
	private function HasValidParent($parent = null)
	{
			//If no parent is given, uses its own already set
		if (!$parent) {
			$parent = $this->parent;
		}

			//null is ok
		if ($parent == null) {
			return true;
		}

		$pages = Pages::LoadAllPages();

			//if parent is found as header
		foreach ((array)$pages as $page) {
			if ($parent == $page->header) {
				return true;
			}
		}

		return false;
	}

}
