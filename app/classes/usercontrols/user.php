<?php

class User extends DBitems
{
    public $Name = null;
    private $Group = null;
    private $Password = null;
    private $PasswordHash;

    //If name is given, load from DB. Otherwise try to load from session
    function __construct($name = null) 
    {
        //Calls DbItems constructor to set upp the mapper
        parent::__construct();
        
        //Tries to load user from the database
        if($name)
        {
            $this->SetName($name);
            $this->Load();
        }             
    }

    function SetName($name)
    {
        if(!$name) { throw new Exception( "No name given" ); }

        $this->Name = $name;
        return true;
    }

    function SetPassword($password)
    {
        if(!$password) { throw new Exception( "No name given" ); }

        $this->Password = $password;
        return true;
    }

    //Tries to load a current user from the session-data
    function LoadCurrentUser()
    {
        $f3 = Base::Instance();

        $this->Name = $f3->get('SESSION.userName');
        $this->Group = $f3->get('SESSION.group');

        if(!$this->Name || !$this->Group) { return false; }

        return true;
    }

    public function VarifyGroup()
    {
        throw new Exception("Not implemented");
    }

    public function LoginUser()
    {
        if(!$this->Name || !$this->Group) { return false; }

        $this->StoreUserInSession();
    }

    public function Load()
    {

        $user = $this->LoadByField("user_id",$this->Name);
          
        //Load from DB
        $this->Name = $user->user_id;
        $this->Group = $user->group;
        $this->PasswordHash = $user->password;

  

        //Checks that the data got loaded
        if(!$this->Name || !$this->Group || !$this->PasswordHash) 
        { 
            throw new Exception("Loading from database failed"); 
        }

        return true;
    }

    public function StoreUserInSession()
    {
        $f3 = Base::Instance();

        $f3->set('SESSION.userName',$this->Name);
        $f3->set('SESSION.group',$this->Group);

        if(!$f3->get('SESSION.userName') || !$f3->get('SESSION.group')) 
        {
            throw new Exception("Storing usedata in session failed");
        }

        return true;
    }

    public function Save()
    {
        if(!$this->Name)
        {
            throw new Exception("No user given");
        }
        if(!$this->Password)
        {
            throw new Exception("No password given");
        }
        
        $this->mapper->user_id = $this->Name;
		$hashedPass = UserControls::HashPassword($this->Password);
		$this->mapper->password = $hashedPass; 

		try {
			$this->mapper->save();
		} catch (Exception $e) {
			return false;
        }
        
        return true;
    }    

    public function VarifyPassword($password)
	{
		$crypt = Bcrypt::instance();
        $result = $crypt->verify($password, $this->PasswordHash);
		return $result;
	}

//Sets $this->db
	protected function SetDbPath()
	{
		$this->dbPath = "./db/simultimatestuser.db";
	}
//Sets $this->mapper
	protected function SetTableName()
	{
		$this->tableName = "users";
	}
//Sets $this->startSql
	protected function SetStartSql()
	{
		$this->schemaSql =
			'CREATE TABLE IF NOT EXISTS "' . $this->tableName . '" (
		`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
		`user_id` TEXT NOT NULL UNIQUE,
		`password` TEXT NOT NULL,
		`group` TEXT NOT NULL DEFAULT "Unvalidated"
		)';
	}
	//Sets the object
	protected function SetData(...$args)
	{

	}
}

?>
