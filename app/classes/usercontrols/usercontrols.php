<?php

class UserControls
{
	//Propmts for login, if group is given checks that the user fulfills the requirment. Otherwise all is allowed.
	public static function LoginRequired(array $Groups = null)
	{
		$f3 = base::instance();

		$user = new User();
	
		if($user->LoadCurrentUser())
		{
		
			//Check group rights

		}
		else
		{
			//Saves the requested page in session for later redirect
			$f3->set('SESSION.redirect',$f3->hive()['PARAMS'][0]);

			//Prompt login
			header('Location:/login'); 
		}

	}

	public static function LoginUser($givenUserName, $givenPassword)
	{
		$f3 = Base::Instance();

		$user = new User($givenUserName);

		if($user->VarifyPassword($givenPassword))
		{
			$user->StoreUserInSession();
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function LogOut()
	{
		$f3 = Base::instance();

		$f3->clear('SESSION.userName');
        $f3->clear('SESSION.group');
		// session_commit();
	}

	public function AddUser($username, $password)
	{
		$user = new User();
		$user->SetName($username);
		$user->SetPassword($password);
		
		if(!$user->Save())
		{
			return false;
		}

		return true;
	}

	// Returns a hashed password
	public static function HashPassword($password)
	{
		$crypt = Bcrypt::instance();
		$hash = $crypt->hash($password);
		return $hash;
	}

	//Checks if there is a logged in user in the session, if so returns username
	public static function IsLoggedIn($f3)
	{
		$userName = $f3->get('SESSION.userName');
		if(!$userName)
		{
			return false;
		}

		return $userName;
	}
}
?>
