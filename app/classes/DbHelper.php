<?php
/**
*
*/
class DbHelper
{
	private $db = null;

	function __construct($dbPath)
	{
		// if(!is_dir($dbPath)) { throw new Error("Database not found"); }
		$this->db = new DB\SQL("sqlite:".$dbPath);
	}

	//Runs a given query on a given db-connection
	public function RunQuery($sql)
	{
		$data = $this->db->exec($sql);
		// if(empty($data)) { throw new Error("No response to query"); }
		return $data;
	}

}


?>
