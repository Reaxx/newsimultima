<?php
abstract class DBitems
{
	protected $f3;
	protected $dbPath;
	protected $db;
	protected $mapper;
	protected $schemaSql;
	protected $tableName;

	public function __construct(...$args)
	{
		date_default_timezone_set("Europe/Stockholm");
		$this->setData(...$args);
		$this->SetDbPath();
		$this->SetTableName();
		$this->SetStartSql();

		//Connects (and creates) to the database
		$this->ConnectAndCreateDB();
	}

	//Uses the mapper
	public function LoadByField($fieldName, $critera)
	{
		$data=$this->mapper->load(array($fieldName.'=?', $critera));
		if(empty($data)) { return FALSE; }

		return $data;
	}
	public function Remove()
	{
		if($this->Load())
		{
			$this->mapper->erase();
		}
		else
		{
			throw new Exception("Data not found");
		}
	}

	private function ConnectAndCreateDB()
	{
		$this->CreateDB();
		$this->CreateTableAndMapper();

		if(empty($this->dbPath)) { throw new Error ("Database not set");}
		if(empty($this->tableName)) { throw new Error ("Tablename not set");}
		if(empty($this->schemaSql)) { throw new Error ("SQL schema not set");}
	}

	protected function CreateDB()
	{
		$path = dirname($this->dbPath);
		if(!is_dir($path)) { mkdir($path); }
		$this->db = new DB\SQL("sqlite:".$this->dbPath);
	}
	protected function CreateTableAndMapper()
	{
		$this->db->exec($this->schemaSql);
		$this->mapper=new DB\SQL\Mapper($this->db,$this->tableName);
		//Turns on PDO exceptions
		$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	}


	//Sets $this->db
	abstract protected function SetDbPath();
	//Sets $this->mapper
	abstract protected function SetTableName();
	//Sets $this->startSql
	abstract protected function SetStartSql();
	//Sets the object
	abstract protected function SetData(...$args);

	abstract protected function Load();
	abstract protected function Save();

}

?>
