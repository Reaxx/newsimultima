<?php
//Errors get stored in LastError
class Discord
{
    private $WebHookUrl = null;
    public $LastError = null;

    public function __construct($webHook)
    {
        if (!$webHook) {
            throw new Exception("URL can not be null");
        }
        $this->WebHookUrl = $webHook;
    }

    //Sends messege to Discord webhook
    public function Send($message, $includeTimestamp = false)
    {
        $data = $this->FormatData($message, $includeTimestamp);
        $ch = $this->SetCurl($data);

        try {
            $this->RunCurl($ch);
        } catch (Throwable $th) {
            return false;
        }

        return true;
    }

    private function FormatData($data, $includeTimeStamp)
    {
        if (empty($data)) {
            throw new Exception("No data given");
        }

        $data = htmlentities($data);

        if ($includeTimeStamp) {
                $data = date("Y-m-d H:i:s") . ": " . $data;
            }
        $data = array('content' => $data);

        return json_encode($data);
    }

    private function RunCurl($ch)
    {
        $result = curl_exec($ch);

        if ($result === FALSE) {
            $msg = curl_error($ch);
            $this->LastError = $msg;
            throw new Exception($msg);
        }
    }

    private function SetCurl($data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->WebHookUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        return $ch;
    }
}
