<?php


/**
*
*/
class SverokAPI
{
	public $ApiKey;
	public $AssocNr;

	function __construct($f3)
	{
		$configpath = "../app/config/config.ini";
		// $f3->BASE."/app/config.ini";

		//Gets ApiKey and AssocationNumber from the config
		$this->ApiKey = ParseIni::Get("sverok","APIKEY",$configpath);
		$this->AssocNr = ParseIni::Get("sverok","ASSOCNR");

		$this->MapValues($f3);
	}

	private function MapValues($f3)
	{
		//Maps the values from the form
		$this->firstname = $f3->POST["firstname"];
		$this->lastname = $f3->POST["lastname"];
		$this->co = $f3->POST["co"];
		$this->socialsecuritynumber = $f3->POST["socialsecuritynumber"];
		$this->email = $f3->POST["email"];
		$this->phone = $f3->POST["phone"];
		$this->street = $f3->POST["street"];
		$this->zip_code = $f3->POST["zip_code"];
		$this->city = $f3->POST["city"];
		$this->nick = $f3->POST["nick"];
		$this->member_fee = $f3->POST["member_fee"];

		//Temporary soultion until Sverok adds support to nick in the Api
		if($this->nick)
		{
			$this->lastname .= " (".	$this->nick.")";
		}

	}

	function Confirm($f3)
	{
		$request = array
		(
			"request" => array
			(
				"action" => "confirm_membership",
				"association_number" => $this->AssocNr,
				"api_key" => $this->ApiKey,
				"year_id" => date("Y"),
				"socialsecuritynumber" => $this->socialsecuritynumber
				)
			);

			$url = "https://ebas.sverok.se/apis/confirm_membership.json";
			$result = $this->MakeCurlCall($request,$url);

			//If memeber is found
			if($result->response->member_found)
			{
				return true;
			}

			//IF error is found
			if($result->response->request_result->error)
			{
				return $result->response->request_result->error;
			}

			return false;
		}

		//Returns true if successful, array with errors if fail
		//Returns true if successful, array with errors if fail
		function AddMember($f3)
		{
			$request = array
			(
				"api_key" => $this->ApiKey,
				"member" => array
				(
					"firstname" => $this->firstname,
					"lastname" => $this->lastname,
					"renewed" => date('Y-m-d'),
					"co" => $this->co,
					"socialsecuritynumber" => $this->socialsecuritynumber,
					"email" => $this->email,
					"phone1" => $this->phone,
					"street" => $this->street,
					"zip_code" => $this->zip_code,
					"city" => $this->city,
					"member_fee" => $this->member_fee
					)
				);

				$url = "https://ebas.sverok.se/apis/submit_member.json";
				$result = $this->MakeCurlCall($request,$url);

				if($result->request_result == "success")
				{
					return true;
				}
				else
				{
					return $result->member_errors;
				}
			}

			//Makes the Curl-connection and returns any answer
			private function MakeCurlCall($request,$url)
			{
				$data_string = json_encode($request);

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);

				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);

				$result=curl_exec($ch);
				curl_close($ch);
				return json_decode($result);
			}
		}
		?>
