<?php

class LoginController
{
    const MaxAttempts = 3;
    const CutOfFTime = 60; //Seconds


    function LoginWindow($f3)
    {
        // Checks number of failed attempts
        $failedAttempts = $f3->get('SESSION.failedLogInAttempts');
        $loginCutOffTime = $f3->get('SESSION.loginCutOff');

        // Checks how long since last failed loginattempt
        if($loginCutOffTime) {
        $timeSinceCutOff = (new DateTime())->getTimestamp() - $loginCutOffTime->getTimestamp();
        // True if long enugh to allow a new attempt
        $hasCutOffPassed = $timeSinceCutOff >= self::CutOfFTime;
        }
 

        if($failedAttempts < self::MaxAttempts)
        {
            echo \Template::instance()->render('\login.html');
        }
        //Resets counter and allows user to try to login again
        else if ($hasCutOffPassed)
        {
            //Resets failed loginattepmts
            $f3->clear('SESSION.failedLogInAttempts');
            echo \Template::instance()->render('\login.html');
        }
        else
        {            
            $timeLeft = self::CutOfFTime-$timeSinceCutOff;
            die("To many failed login-attempts (".$timeLeft.")");
        }
    }

    function LoginUser($f3)
    {
        $username = $f3->get("POST.username");
        $password = $f3->get("POST.password");

        //If login works
        if(UserControls::LoginUser($username,$password))
        {
            //Retrives and cleares the redirect session.
            $redirectPaht = $f3->get('SESSION.redirect');
            $f3->clear('SESSION.redirect');

            //Resets failed loginattepmts
            $f3->clear('SESSION.failedLogInAttempts');

            header('Location:'.$redirectPaht); 
        }
        //If login fails
        else
        {   
            //Retrieves the number of failed logins
            $prevFailedAttempts = $f3->get('SESSION.failedLogInAttempts');

            //Setts time of last attempt
            $f3->set('SESSION.loginCutOff',new DateTime());
     
            //Increments failed login attepmts
            $prevFailedAttempts++;
            $f3->set('SESSION.failedLogInAttempts',$prevFailedAttempts);

            //Redraws the login prompt
            header('Location:/login'); 
        }
    }

    function LogOut($f3)
    {
        UserControls::LogOut();
    }
}
?>