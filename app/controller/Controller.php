<?php
class Controller
{
	function ShowPage($f3)
	{
		$pageTitle = $f3["PARAMS"]["page"];

		$pageContent = $this->LoadPageContent($pageTitle,$f3);
		$this->FormatRelatedPages($pageContent);

		$f3->set("page", $pageContent);

		// Include header before route
		echo Template::instance()->render('defaults/header.html');
		echo \Template::instance () -> render('pagetemplate.html');
		echo Template::instance()->render('defaults/footer.html');
	}

	function Preview($f3)
	{
		$parent = $f3["PARAMS"]["parent"];
		//Loads the pagecontent
		$page = $this->LoadPreview($parent);

		$f3->set("page", $pageContent);

		var_dump($page);

		echo Template::instance()->render('defaults/header.html');
		echo \Template::instance () -> render('preview.html');
		echo Template::instance()->render('defaults/footer.html');
	}

	private function LoadPreview($parent)
	{
		$page = new Page();
		$page->parent = $parent;
		$page->GetRaltedPages();

		$page->rootPages = Pages::GetRootPages();

		return $page;

	}

	function PHPinfo()
	{
		phpinfo();
	}

	//Gets the data from the memberform
	function SverokApiCall($f3)
	{
		$APIcon = new SverokAPI($f3);

		//Checks if the socialsecuritynumber is already in the system
		if(($response = $APIcon->Confirm($f3)) === true)
		{
			$error[] = "Personummret finns redan i databasen";
		}
		//If error is present
		else if($response)
		{
			$error[] = $response;
		}
		else
		{
			if(($erros = $APIcon->AddMember($f3)) === true)
			{
					$f3->set("msg", "Medlem tillagd i databasen, glöm inte att också betala in angiven medlemsavgift.");
			}
			else
			{
				foreach($erros as $er)
				{
					$error[] =  $er[0];
				}
			}
		}

		//Sends error to the view, and redwards the form
		if(!empty($error))
		{
			$f3->set("errors", $error);
		}

		//Sends formdata back to the form
		$f3->set("formData", $f3->POST);

		$this->DrawPage("Bli medlem",$f3);

	}

	//Quick and dirty way to redraw the window
	public function DrawPage($pageHeader,$f3)
	{
		$params["page"]=$pageHeader;

		$this->beforeroute($f3, $params);
		$this->ShowPage($f3);
		$this->afterroute($f3);
	}

	private function LoadPageContent($pagetitel,$f3)
	{
		$page = new Page();
		$page->header = $pagetitel;

		//Loads page, if fail. Try to find fallback instead.
		if(!$page->load())
		{
			$page->header = ParseIni::Get("config","fallbackPage");

			//If loading of index fails, throw error
			if(!$page->Load())
			{
				throw new Exception("No fallback page found, expcted ".$page->header);
			}
		}

		//Prepare rootpages for the meny
		$page->rootPages = Pages::GetRootPages();

		return $page;
	}

	private function FormatRelatedPages($page)
	{
		//Loads childpages
		$page->GetRaltedPages();

		//If page has no children or siblings
		if(count(array($page->children)) == 0)
		{
			$page->menuheader = "Välkommen";
			$page->children[0]["header"] = "Använd menyn ovan för att navigera sidan. Fler menyalternativ kommer efterhand presenteras här.";
		}
		else
		{
			if($page->IsRootPage())
			{
				$page->menuheader = $page->header;
			}
			else
			{
				$page->menuheader = $page->parent;
			}
		}
	}

}
?>
