<?php

/**
*
*/
class PageAdminController
{

	function beforeroute($f3)
	{
		UserControls::LoginRequired();	
	}


	function afterroute($f3)
	{
	
	}

	//Draws the adminpage to the screen
	function DrawPage($f3)
	{
		$pageList = self::MakeListOfPages($f3);
		$f3->set("pageslist",$pageList);

		echo Template::instance()->render('defaults/header.html');
		echo \Template::instance () -> render('pageadmin.html');
		echo Template::instance()->render('defaults/footer.html');
	}

	function Copy($f3)
	{
		$page = new Page();
			$f3->set('content',$f3->get("POST.content"));

			self::DrawPage($f3);
	}

	//Saves a changed page
	// function Update($f3)
	// {
	// 	$page = new Page();
	// 	$page->id = trim($f3->get("POST.id"));
	// 	$page->Load();

	// 	//Saves the new data to an object
	// 	$newdata = new stdClass();
	// 	$newdata->header = $f3->get("POST.header");
	// 	$newdata->parent = $f3->get("POST.parent");
	// 	$newdata->content = $f3->get("POST.content");

	// 	//Saves the page with new data
	// 	if(!$page->Save($newdata))
	// 	{
	// 		throw new Exception("Failed to save to database");
	// 	}

	// 	//When done, opens again-
	// 	$path = 'Location:/AdminPage/'.$newdata->header;
	// 	header($path); 
	// }
	//Loads a page from the database
	// function Open($f3)
	// {		
	// 	$page = new Page();

	// 	//Can't remeber why I added this
	// 	if($f3->REQUEST["id"])
	// 	{
	// 		echo "Notice!!";
	// 		//Reppens old page
	// 		$page->id = $f3->REQUEST["id"];
	// 	}
	// 	//If pagename has been set
	// 	else if(!empty($f3->get('PARAMS.pagename')))
	// 	{
	// 			$page->header = $f3->get('PARAMS.pagename');
	// 	}
	// 	//if pageid
	// 	else
	// 	{
	// 		$page->id = $f3->get('PARAMS.pageid');
	// 	}

	// 	//Return 404 when no page was found
	// 	if(!$page->Load())
	// 	{
	// 		$f3->error("404");
	// 	}

	// 	$f3->set('id',$page->id);
	// 	$f3->set('parent',$page->parent);
	// 	$f3->set('header',$page->header);
	// 	$f3->set('content',$page->content);


	// 	self::DrawPage($f3);
	// }
	//Removes a page from the database
	function Delete($f3)
	{
		$page = new Page();
		$page->id = trim($f3->get("POST.id"));
		$page->Remove();

		self::DrawPage($f3);
	}

	private function MakeListOfPages($f3)
	{
		$pages = Pages::GetSortedPageList();

		//Sets the publicname for indented levels
		if($pages)
		{
		foreach ($pages as $page)
		{
				for ($i=0; $i < $page->level-1; $i++)
				{
					$page->publicName .= "-";
				}
				$page->publicName .= "| ".$page->header;
		}
		
		return $pages;
		}
	}

	public function OpenJson($f3)
	{
	$tmp = new Page();
	$tmp->id = $f3->get('PARAMS.pageid');
	$tmp->Load();
	echo json_encode($tmp);	
	}

//Tries to update the database, and then returns a jsonObject with the new pages. If error, will sett "error" to errormessage
public function UpdateJson($f3)
	{
	$page = new Page();
	//Tries to load the page
	$page->id = trim($f3->get("PARAMS.pageid"));
	$page->Load();

	// Saves the page with new data
	try {
		$page->SaveFromPost();
	
		// After update, responds with new pagelist
		$pageList = self::MakeListOfPages($f3);
	
	} catch (Exception $error) {
		$pageList = self::MakeListOfPages($f3);
		$pageList["error"] = $error->getMessage();
	} finally {
		echo json_encode($pageList);
	}
	}
}
?>
