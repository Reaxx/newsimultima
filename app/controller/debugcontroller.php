<?php

class DebugController
{
    function beforeroute($f3)
    {
        //Enables loginpromt
        UserControls::LoginRequired( array ("Admin") );
    }

    function LogOut()
    {
        UserControls::LogOut();
    }

    function AddUser($f3)
    {
        $username = $f3->get('PARAMS.name');
        $password = $f3->get('PARAMS.pass');

        $user = new UserControls();

        if ($user->AddUser($username, $password)) {
                echo "User " . $username . " added";
            } else {
                echo "Failed";
            }
    }

    function AddPage($f3)
    {
        // @parent/@header/@content

        $page = new Page();
        $page->setHeader($f3->get("PARAMS.header"));
        $page->SetParent($f3->get("PARAMS.parent"));
        $page->SetContent($f3->get("PARAMS.content"));

        if($page->Save())
        {
            echo json_encode($page);
           
        }
        else {
            throw new Exception("Saving the page failed");
        }
    }

    function Discord($f3)
    {
        ParseIni::SetIni("./app/config/config.ini");
        $webHook = ParseIni::Get("discord","webhookUrl");
        $msg = $f3->get('PARAMS.messege');
        $msg = $msg." by ".UserControls::IsLoggedIn($f3);

        $discord = new Discord($webHook);
        if($discord->Send($msg, true))
        {
            echo "Message sent";
        }
        else {
            echo $discord->LastError;
        }
    }
}

?>