class PageData {

    static LoadPage(id) {
        let url = "/AdminPage/OpenJson/" + id;
        //Making the ftech
        let data = this.GetDataAsync(url);
        //Waitning for answer, then putting data in the form.
        data.then(data => this.WriteToForm(data));
    }

    //Updates data in database and updates LinkList
    static UpdatePage() {
        //Validates the form
        if(!document.getElementById("pageform").reportValidity())
        {
            return false;
        }

        //Disables the button and sets text to "updaing" 
        this.SetUpdateButtonText("Updating", true);

        let form = document.getElementById("pageform");
        let data = new URLSearchParams(new FormData(form))

        let id = data.get("id");
        let url = "/AdminPage/UpdateJson/" + id;

        //Gets data from backend
        let response = this.SendDataAndWaitForResponseAsync(url, data);
        //Sends data to list
        response.then(response => this.WriteToList(response));
    }

    static DeletePage(id) {

    }

    static async GetDataAsync(url) {
        let respone = await fetch(url);
        let data = await respone.json();
        return data;
    }

    static async SendDataAndWaitForResponseAsync(url, postData) {
        let respone = await fetch(url, { method: 'POST', body: postData });
        let data = await respone.json();

        return data;
    }

    static WriteToForm(data) {
        // console.log(data);
        document.getElementsByName("id")[0].value = data.id;
        document.getElementsByName("parent")[0].value = data.parent;
        document.getElementsByName("header")[0].value = data.header;
        document.getElementsByName("content")[0].value = data.content;
    }

    static WriteToList(data) {
        //Turns the object into array for easeir iteration
        let dataArray = Object.entries(data);
 
        //Gets the last element to check if its an error
        let lastElement = dataArray[dataArray.length -1]

          //checks if error was returned
        if(lastElement[0] == "error")
        {
            //removes error from array
            dataArray.splice(-1);

            //Sends error to handeling
            this.HandleUpdateErrors(lastElement[1]);
        }
        else {
            //Resets button
            this.SetUpdateButtonText("Updated", false);
        }

        let list = document.getElementsByTagName("linklist")[0];

        //Empty list
        list.innerHTML = "";

        //Adds the divs to the list
        dataArray.forEach(element => {
                //Rebuilds the linklist
                this.AddPageToList(list, element);     
        });

        //Adds the "new" div
        let div = document.createElement("div");
        div.innerText = "New (empty Form)";
        div.onclick = function () { PageData.ClearForm(); };
        list.appendChild(div);  
    }

    //Takes the data, parses and appends to the list,
    static AddPageToList(list, element) {
        element = element[1];
        let div = document.createElement("div");
        div.innerText = element.publicName;
        div.onclick = function () { PageData.LoadPage(element.id); };
        list.appendChild(div);
    }

    static ClearForm() {
        document.getElementById("pageform").reset();
    }

    //
    static HandleUpdateErrors(error) {
        console.log(error);
        let button = document.getElementById("updateButton");
        button.title = error;
        button.textContent = "Error";
        button.style = "color:red; background-color: purple";
     }
    
    static SetUpdateButtonText(text, disable = false)
    {
        let button = document.getElementById("updateButton");
        button.title = text;
        button.disable = disable;
        button.textContent = text;
        button.style = "color:black; background-color: green";
    }
}