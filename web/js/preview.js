class Preview
{

	static previewPage()
	{
		var iframeDiv = document.createElement('div');
		iframeDiv.setAttribute("id","iframeContainer");

		var iframe = document.createElement('iframe');
		iframe.setAttribute("id", "preview");

		//Fetches the parent from the form and sens on to the page to load the menu
		var parent = document.getElementsByName("parent")[0].value;
		iframe.src="/preview/"+parent;
		iframeDiv.appendChild(iframe);

		// Cretes buttons
		var confirmBtn = document.createElement("BUTTON");
		confirmBtn.setAttribute("id","confrimPreview");
		confirmBtn.setAttribute("type","submit");
		confirmBtn.setAttribute("name","updateButton");
		confirmBtn.setAttribute("class","green");
		confirmBtn.setAttribute("onclick","Preview.save()");
		confirmBtn.setAttribute("formaction","/AdminPage/Update");
		confirmBtn.appendChild(document.createTextNode('Save changes'));
		iframeDiv.appendChild(confirmBtn);

	//
		// Cretes buttons
		var backBtn = document.createElement("BUTTON");
		backBtn.setAttribute("id","confrimPreview");
		backBtn.setAttribute("type","button");
		backBtn.setAttribute("onclick","Preview.close()");
		backBtn.appendChild(document.createTextNode('Exit preview'));
		iframeDiv.appendChild(backBtn);


		document.body.appendChild(iframeDiv);
		//removeChild
	}
	static save()
	{
		var form = document.getElementById('pageform');
		form.setAttribute("action","/AdminPage/Update");
		document.getElementById('pageform').submit();
	}

	static close()
	{
		document.getElementById("iframeContainer").remove();
	}

	static loadContent()
	{
		// Creates the iframe
		try
		{
			var content = window.parent.document.getElementsByName("content")[0].value;

		}
		catch (e)
		{

		}

		document.getElementsByTagName("main")[0].innerHTML = content;
	}
}
