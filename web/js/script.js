function showElementByName()
{
	var length = arguments.length;
	for (i = 0; i < length; i++)
	{
		document.getElementsByName(arguments[i])[0].removeAttribute("style");
	}
}

function hideElementByName()
{
	var length = arguments.length;
	for (i = 0; i < length; i++)
	{
		document.getElementsByName(arguments[i])[0].setAttribute("style","grid:none");
	}
}

function editIMG()
{
	var url = prompt("Choose img", "img.png");

	var content="[IMG]"+url;
	document.getElementsByName("content")[0].value += content;
	document.getElementsByName("content")[0].focus();
}
